import { Component, OnInit, ViewEncapsulation, Input } from '@angular/core';
import { HttpService } from '../http.service';

@Component({
  selector: 'app-chartbar',
  template: `


        <button (click)='getdata()'>Reset</button>
        <div>
        <div  class='example-loading-shade' *ngIf="isLoadingResults">
        <div *ngIf="isLoadingResults">{{err}}</div>
      </div>
        <div style="display: block " *ngIf="barChartData.length > 0">    
        <canvas baseChart
                    [labels]='barChartLabels'
                    [datasets]="barChartData"
                    [chartType]="barChartType"
                    [colors]="barChartColor">
                  </canvas>
      </div>
  </div>

  `,
  styles: [
    `
    .example-loading-shade {
      position: absolute;
      top: 0;
      left: 0;
      bottom:0;
      right: 0;
      background: rgba(0, 0, 0, 0.15);
      z-index: 1;
      display: flex;
      align-items: center;
      justify-content: center;
    }
    .card-titles {
      margin-left: 30px;
    }
    .heaaders {
      margin: 0 15px;
      padding: 0;
      position: relative;
    }
    `
  ],
  encapsulation: ViewEncapsulation.Native
})
export class ChartBarComponent{

constructor(private http:HttpService) { }
length:number;
  labels:Array<any>;
  barChartLabels: any;
  barChartType: any;
  barChartData:any;
  barChartColor:Array<any>;
  isLoadingResults = true;
  err:string;
  @Input() url=''

  ngOnInit() {
    this.getbar()
   this.getdata();
  }
  getdata(){
    this.http.getchartBar(this.url)
    .subscribe(
      (res) => {
        console.log(res)
        this.length=res.length;
       this.barChartLabels=res[0].label;
       this.barChartType=res[0].type;
       console.log(this.barChartType)
       this.isLoadingResults = false;
       var datachart=[]
        for(let i=0;i<res.length;i++){
          //length 3
          datachart.push({
            data: res[i].data,
            label: res[i].name,
           
          })
        }
         this.barChartData=datachart
      },
      (err) => {
        this.isLoadingResults = true;
        this.err=err;
      
      },
      () => {
        this.isLoadingResults = false;
        
      }
    );
  } 
  
   getbar(){
  
    this.barChartLabels= this.barChartLabels;
    this.barChartData=[]
    
     this.barChartColor = [];
      var colors = ['#28a745', '#145A32', '#008080', '#17a2b8', '#007bff', '#6610f2', '#e83e8c', '#dc3545', '#fd7e14', '#ffc107']
      for (let i = 0; i < 10; i++) {
        this.barChartColor.push({
          backgroundColor: colors[i]
        });
      }
  }  

}
