import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
interface itembar{
  id:number;
  name:string;
  label:string[];
  data:number[];
  type:string;
}
@Injectable({
  providedIn: 'root'
})
export class HttpService {

  constructor(private http:HttpClient) { }

  getchartBar(url): Observable<any> {
    console.log(url)
    return this.http.get<itembar>(url)
  }
}
