import { Injector, NgModule } from '@angular/core';
import { createCustomElement } from '@angular/elements';
import { BrowserModule } from '@angular/platform-browser';
import { ChartModule } from 'angular2-chartjs';
import { ChartBarComponent } from './chartBar/chartBar.component';
import { HttpClientModule } from '@angular/common/http';
import { ChartsModule } from 'ng2-charts';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
@NgModule({
  declarations: [ChartBarComponent],
  imports: [BrowserModule,ChartModule,HttpClientModule,ChartsModule,FontAwesomeModule, NgbModule.forRoot()],
  entryComponents: [ChartBarComponent]
})
export class AppModule {
  constructor(injector: Injector) {
    const customButton = createCustomElement(ChartBarComponent, {
      injector: injector,
     });
     customElements.define('app-chartbar', customButton);
  }

  ngDoBootstrap() {}
}
